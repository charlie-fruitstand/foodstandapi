package com.fruitstandapi.fruitstandapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FruitstandapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(FruitstandapiApplication.class, args);
	}

}
