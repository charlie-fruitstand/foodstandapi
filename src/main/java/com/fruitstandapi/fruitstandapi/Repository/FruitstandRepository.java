package com.fruitstandapi.fruitstandapi.Repository;

import com.fruitstandapi.fruitstandapi.Models.Fruitstand;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FruitstandRepository extends JpaRepository<Fruitstand, Long> {
}
