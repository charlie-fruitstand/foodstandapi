package com.fruitstandapi.fruitstandapi.Models;

import javax.persistence.*;

@Entity
@Table(name="api_db")
public class Fruitstand {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false, length = 150)
    private String name;

    @Column(nullable = false)
    private String description;

    @Column(nullable = false)
    private String location;

    @Column(nullable = false)
    private String color;

    @Column(nullable = false)
    private String shape;

    public Fruitstand() {
    }

    public Fruitstand(long id, String name, String description, String location, String color, String shape) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.location = location;
        this.color = color;
        this.shape = shape;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getShape() {
        return shape;
    }

    public void setShape(String shape) {
        this.shape = shape;
    }

}
