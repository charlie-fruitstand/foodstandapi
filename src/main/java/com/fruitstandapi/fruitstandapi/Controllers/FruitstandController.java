package com.fruitstandapi.fruitstandapi.Controllers;

import java.util.List;

import com.fruitstandapi.fruitstandapi.Models.Fruitstand;
import com.fruitstandapi.fruitstandapi.Repository.FruitstandRepository;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;

@RestController
@RequestMapping(value = "fruitstand")
@CrossOrigin
public class FruitstandController {
    private final FruitstandRepository fruitstandRepo;

    public FruitstandController(FruitstandRepository fruitstandRepo) {
        this.fruitstandRepo = fruitstandRepo;
    }

    @GetMapping("/fruits")
    public String viewAll(Model model) {
        List<Fruitstand> fruitList = fruitstandRepo.findAll();
        model.addAttribute("noFruitsFound", fruitList.size() == 0);
        model.addAttribute("fruits", fruitList);
        return "/fruits";
    }

    @GetMapping("/fruits/add-fruit")
    public String showFruit(Model viewModel) {
        viewModel.addAttribute("fruit", new Fruitstand());
        return "/fruits/add-fruit";
    }

    @PostMapping("/fruits/add-fruit")
    public String newFruit(@ModelAttribute Fruitstand fruitToBeSaved) {
        fruitstandRepo.save(fruitToBeSaved);
        return "redirect:/books";
    }

    @GetMapping("/fruits/{id}")
    public String viewFruit(@PathVariable long id, Model model) {
        Fruitstand fruitstand = fruitstandRepo.getOne(id);
        model.addAttribute("fruitId", id);
        model.addAttribute("fruit", fruitstand);
        return "/fruits/view";
    }

    @GetMapping("/fruits/{id}/edit")
    public String showEditFruit(Model model, @PathVariable long id) {
        Fruitstand editThisFruit = fruitstandRepo.getOne(id);
        model.addAttribute("fruitEdit", editThisFruit);
        return "/fruits/{id}/edit";
    }

    @PostMapping("/fruits/{id}/edit")
    public String updateFruit(@PathVariable long id, 
                            @RequestParam(name = "name") String name,
                            @RequestParam(name = "description") String description,
                            @RequestParam(name = "location") String location,
                            @RequestParam(name = "color") String color,
                            @RequestParam(name = "shape") String shape
                            ) {
        Fruitstand foundFruit =fruitstandRepo.getOne(id);
        foundFruit.setName(name);
        foundFruit.setDescription(description);
        foundFruit.setLocation(location);
        foundFruit.setColor(color);
        foundFruit.setShape(shape);
        return "redirect:/fruits";
    }

    @PostMapping("/fruits/{id}/remove")
    public String removeFruit(@PathVariable long id) {
        fruitstandRepo.deleteById(id);
        return "redirect:/fruits";
    }

}
